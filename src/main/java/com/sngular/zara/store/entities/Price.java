package com.sngular.zara.store.entities;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@Table(name="prices")
public class Price {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, updatable = false, nullable = false)
    private Long id;

    @Column(name = "brand_id")
    private Integer brandId;

    @Column(name = "start_date")
    private LocalDateTime startDatePrice;

    @Column(name = "end_date")
    private LocalDateTime endDatePrice;

    @Column(name = "price_list")
    private Integer priceList;

    @Column(name = "product_id")
    private Long productId;

    @Column(name = "priority")
    private Integer priorityPrice;

    @Column(name = "price")
    private BigDecimal price;

    @Column(name = "curr")
    private String currency;

    public Price() {
    }

    public Price(Long id, Integer brandId, LocalDateTime startDatePrice, LocalDateTime endDatePrice, Integer priceList, Long productId, Integer priorityPrice, BigDecimal price, String currency) {
        this.id = id;
        this.brandId = brandId;
        this.startDatePrice = startDatePrice;
        this.endDatePrice = endDatePrice;
        this.priceList = priceList;
        this.productId = productId;
        this.priorityPrice = priorityPrice;
        this.price = price;
        this.currency = currency;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getBrandId() {
        return brandId;
    }

    public void setBrandId(Integer brandId) {
        this.brandId = brandId;
    }

    public LocalDateTime getStartDatePrice() {
        return startDatePrice;
    }

    public void setStartDatePrice(LocalDateTime startDatePrice) {
        this.startDatePrice = startDatePrice;
    }

    public LocalDateTime getEndDatePrice() {
        return endDatePrice;
    }

    public void setEndDatePrice(LocalDateTime endDatePrice) {
        this.endDatePrice = endDatePrice;
    }

    public Integer getPriceList() {
        return priceList;
    }

    public void setPriceList(Integer priceList) {
        this.priceList = priceList;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public Integer getPriorityPrice() {
        return priorityPrice;
    }

    public void setPriorityPrice(Integer priorityPrice) {
        this.priorityPrice = priorityPrice;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }
}

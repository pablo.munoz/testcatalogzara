package com.sngular.zara.store.services;

import com.sngular.zara.store.dtos.PriceDTO;
import com.sngular.zara.store.entities.Price;
import com.sngular.zara.store.mappers.IPriceResponseViewMapper;
import com.sngular.zara.store.mappers.IPricesMapper;
import com.sngular.zara.store.repositories.IPricesRepository;
import com.sngular.zara.store.views.PriceResponseView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Service
public class PricesService {

    @Autowired
    IPricesRepository iPricesRepository;

    @Autowired
    IPricesMapper iPricesMapper;

    @Autowired
    IPriceResponseViewMapper iPriceResponseViewMapper;

    public Optional<PriceResponseView> getPriceOfProduct(LocalDateTime applicationDate, Integer productId, Integer brandId) {
        Optional<PriceResponseView> optionalPriceResponseView = null;
        Optional<List<PriceDTO>> optionalDtosPrice = null;
        Optional<List<Price>> entitiesPrices = iPricesRepository.findPrices(applicationDate, productId, brandId);

        if(entitiesPrices.isPresent()){
            optionalDtosPrice = Optional.of(iPricesMapper.entityListToDto(entitiesPrices.get()));

            List<PriceDTO> listDtoPrices = optionalDtosPrice.get();
            PriceDTO maxPriorityPriceDto = listDtoPrices.stream().max(Comparator.comparing(PriceDTO::getPriorityPrice)).orElseThrow(NoSuchElementException::new);

            optionalPriceResponseView = Optional.of(iPriceResponseViewMapper.dtoToResponseView(maxPriorityPriceDto));

        }else{
            optionalPriceResponseView = Optional.empty();
        }
        return optionalPriceResponseView;
    }


}

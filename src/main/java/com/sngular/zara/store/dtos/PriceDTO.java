package com.sngular.zara.store.dtos;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public class PriceDTO {
    private Long id;

    private Integer brandId;

    private LocalDateTime startDatePrice;

    private LocalDateTime endDatePrice;

    private Integer priceList;

    private Long productId;

    private Integer priorityPrice;

    private BigDecimal price;

    private String currency;

    public PriceDTO() {
    }

    public PriceDTO(Long id, Integer brandId, LocalDateTime startDatePrice, LocalDateTime endDatePrice, Integer priceList, Long productId, Integer priorityPrice, BigDecimal price, String currency) {
        this.id = id;
        this.brandId = brandId;
        this.startDatePrice = startDatePrice;
        this.endDatePrice = endDatePrice;
        this.priceList = priceList;
        this.productId = productId;
        this.priorityPrice = priorityPrice;
        this.price = price;
        this.currency = currency;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getBrandId() {
        return brandId;
    }

    public void setBrandId(Integer brandId) {
        this.brandId = brandId;
    }

    public LocalDateTime getStartDatePrice() {
        return startDatePrice;
    }

    public void setStartDatePrice(LocalDateTime startDatePrice) {
        this.startDatePrice = startDatePrice;
    }

    public LocalDateTime getEndDatePrice() {
        return endDatePrice;
    }

    public void setEndDatePrice(LocalDateTime endDatePrice) {
        this.endDatePrice = endDatePrice;
    }

    public Integer getPriceList() {
        return priceList;
    }

    public void setPriceList(Integer priceList) {
        this.priceList = priceList;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public Integer getPriorityPrice() {
        return priorityPrice;
    }

    public void setPriorityPrice(Integer priorityPrice) {
        this.priorityPrice = priorityPrice;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }
}

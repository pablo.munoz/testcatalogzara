package com.sngular.zara.store.views;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public class PriceResponseView {

    private Integer brandId;
    private Long productId;
    private Integer priceList;
    private LocalDateTime startDatePrice;
    private LocalDateTime endDatePrice;
    private BigDecimal price;

    public PriceResponseView() {
    }

    public PriceResponseView(Integer brandId, Long productId, Integer priceList, LocalDateTime startDatePrice, LocalDateTime endDatePrice, BigDecimal price) {
        this.brandId = brandId;
        this.productId = productId;
        this.priceList = priceList;
        this.startDatePrice = startDatePrice;
        this.endDatePrice = endDatePrice;
        this.price = price;
    }

    public Integer getBrandId() {
        return brandId;
    }

    public void setBrandId(Integer brandId) {
        this.brandId = brandId;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public Integer getPriceList() {
        return priceList;
    }

    public void setPriceList(Integer priceList) {
        this.priceList = priceList;
    }

    public LocalDateTime getStartDatePrice() {
        return startDatePrice;
    }

    public void setStartDatePrice(LocalDateTime startDatePrice) {
        this.startDatePrice = startDatePrice;
    }

    public LocalDateTime getEndDatePrice() {
        return endDatePrice;
    }

    public void setEndDatePrice(LocalDateTime endDatePrice) {
        this.endDatePrice = endDatePrice;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }
}

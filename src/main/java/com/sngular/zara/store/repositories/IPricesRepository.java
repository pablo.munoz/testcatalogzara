package com.sngular.zara.store.repositories;

import com.sngular.zara.store.entities.Price;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Repository
public interface IPricesRepository extends JpaRepository<Price, Long> {

    @Query(value="SELECT * FROM prices p where p.product_id=:productId and p.brand_id=:brandId and :applicationDate between start_date and end_date", nativeQuery=true)
    Optional<List<Price>> findPrices(LocalDateTime applicationDate, Integer productId, Integer brandId);
}

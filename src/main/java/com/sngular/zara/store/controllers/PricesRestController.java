package com.sngular.zara.store.controllers;

import com.sngular.zara.store.services.PricesService;
import com.sngular.zara.store.views.PriceResponseView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1")
public class PricesRestController {

    @Autowired
    PricesService pricesService;

    @GetMapping("/getPriceOfProduct")
    public ResponseEntity<PriceResponseView> getPriceOfProduct(@RequestParam("fechaDeAplicacion") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime applicationDate,
                                                       @RequestParam("identificadorDeProducto") Integer productId,
                                                       @RequestParam("identificadorDeCadena") Integer brandId){
        if(applicationDate!=null && productId!=null && brandId!=null) {
            Optional<PriceResponseView> priceResponseView = pricesService.getPriceOfProduct(applicationDate, productId, brandId);
            if (priceResponseView.isPresent()) {
                return new ResponseEntity<>(priceResponseView.get(), HttpStatus.OK);
            }else{
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        }
        return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
    }

}

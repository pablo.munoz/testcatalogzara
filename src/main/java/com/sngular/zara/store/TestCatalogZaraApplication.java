package com.sngular.zara.store;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestCatalogZaraApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestCatalogZaraApplication.class, args);
	}

}

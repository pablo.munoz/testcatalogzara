package com.sngular.zara.store.mappers;

import com.sngular.zara.store.dtos.PriceDTO;
import com.sngular.zara.store.views.PriceResponseView;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface IPriceResponseViewMapper {

    IPriceResponseViewMapper INSTANCE = Mappers.getMapper(IPriceResponseViewMapper.class);

    PriceResponseView dtoToResponseView(PriceDTO priceDTO);
}

package com.sngular.zara.store.mappers;

import com.sngular.zara.store.dtos.PriceDTO;
import com.sngular.zara.store.entities.Price;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(componentModel = "spring")
public interface IPricesMapper {

    IPricesMapper INSTANCE = Mappers.getMapper(IPricesMapper.class);

    PriceDTO entityToDto(Price price);

    List<PriceDTO> entityListToDto(List<Price> prices);
}

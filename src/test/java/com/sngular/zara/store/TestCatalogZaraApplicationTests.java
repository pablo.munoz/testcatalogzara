package com.sngular.zara.store;

import com.sngular.zara.store.views.PriceResponseView;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.net.URI;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = TestCatalogZaraApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class TestCatalogZaraApplicationTests {

	private final Logger log = LoggerFactory.getLogger(TestCatalogZaraApplicationTests.class);

	@Autowired
	private TestRestTemplate restTemplate;

	@LocalServerPort
	private int port;

	private String getRootUrl() {
		return "http://localhost:" + port;
	}

	@Test
	@DisplayName("Test 1: petición a las 10:00 del día 14 del producto 35455   para la brand 1 (ZARA)")
	public void test01() {
		String url= "/api/v1/getPriceOfProduct?fechaDeAplicacion=2020-06-14T10:00:00.000&identificadorDeProducto=35455&identificadorDeCadena=1";
		BigDecimal priceExpected = new BigDecimal(35.5);
		Integer priceListExpected = 1;
		test(url,priceExpected,priceListExpected);
	}

	@Test
	@DisplayName("Test 2: petición a las 16:00 del día 14 del producto 35455   para la brand 1 (ZARA)")
	public void test02() {
		String url= "/api/v1/getPriceOfProduct?fechaDeAplicacion=2020-06-14T16:00:00.000&identificadorDeProducto=35455&identificadorDeCadena=1";
		BigDecimal priceExpected = new BigDecimal(25.45);
		Integer priceListExpected = 2;
		test(url,priceExpected,priceListExpected);
	}

	@Test
	@DisplayName("Test 3: petición a las 21:00 del día 14 del producto 35455   para la brand 1 (ZARA)")
	public void test03() {
		String url= "/api/v1/getPriceOfProduct?fechaDeAplicacion=2020-06-14T21:00:00.000&identificadorDeProducto=35455&identificadorDeCadena=1";
		BigDecimal priceExpected = new BigDecimal(35.5);
		Integer priceListExpected = 1;
		test(url,priceExpected,priceListExpected);
	}

	@Test
	@DisplayName("Test 4: petición a las 10:00 del día 15 del producto 35455   para la brand 1 (ZARA)")
	public void test04() {
		String url= "/api/v1/getPriceOfProduct?fechaDeAplicacion=2020-06-15T10:00:00.000&identificadorDeProducto=35455&identificadorDeCadena=1";
		BigDecimal priceExpected = new BigDecimal(30.5);
		Integer priceListExpected = 3;
		test(url,priceExpected,priceListExpected);
	}

	@Test
	@DisplayName("Test 5: petición a las 21:00 del día 16 del producto 35455   para la brand 1 (ZARA)")
	public void test05() {
		String url= "/api/v1/getPriceOfProduct?fechaDeAplicacion=2020-06-16T21:00:00.000&identificadorDeProducto=35455&identificadorDeCadena=1";
		BigDecimal priceExpected = new BigDecimal(38.95);
		Integer priceListExpected = 4;
		test(url,priceExpected,priceListExpected);
	}


	private void test(String url, BigDecimal priceExpected, Integer priceListExpected) {

		String urlFinal=getRootUrl() + url;
		log.info("url: " + url);

		HttpHeaders headers = new HttpHeaders();
		ResponseEntity<PriceResponseView> response = restTemplate.exchange(new RequestEntity<Void>(headers, HttpMethod.GET, URI.create(urlFinal)), PriceResponseView.class);

		assertNotNull(response);
		assertNotNull(response.getBody());
		PriceResponseView priceResponseView = response.getBody();
		BigDecimal priceGet = priceResponseView.getPrice();

		log.info("priceExpected: " + priceExpected.floatValue() + " - priceGet: " + priceGet.floatValue());
		assertEquals(priceExpected.floatValue(),priceGet.floatValue(), 0.001);

		Integer priceListGet = priceResponseView.getPriceList();
		log.info("priceListExpected: " + priceListExpected + " - priceListGet: " + priceListGet);
		assertEquals(priceListExpected.intValue(), priceListGet.intValue());
	}

}
